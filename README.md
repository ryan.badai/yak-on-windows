# Table of contents

[[_TOC_]]

## Description
This repository contains documentation and scripts to run YaK on Windows.
## Installation and usage 

There are two cases :
- If you don't use an hypervisor or use something else than virtualbox, you can use the rancher desktop method (faster and lighter)
- If you are a virtualbox user and don't want to switch to Hyper-V or something else, use the virtualbox virtual machine method.


### Run YaK using Rancher Desktop
Using Rancher is the preferred method if you are an Hyper-V user, a VMWare Workstation user, or if you don't use any hypervisor on your machine.

Rancher desktop is an application that allows you to install the docker daemon in a lightwieght virtual machine automatically and interact with it to run containers through Rancher's GUI or using docker CLI from a powershell terminal.

Notes before installing rancher : 
- This will enable WSL2 and VMP (Virtual Machine Platform, a minimal version of Hyper-V required to run a lightweight VM for WSL2) on your computer if those are not already enabled.

- If you are a Virtualbox user, having the Virtual Machine Platform enabled on your computer will significantly affect your VM's performance, as virtualization extensions are not available to virtualbox because of that. It also prevents NAT networking from working properly in my case. 

- Note that uninstalling rancher desktop doesn't disable WSL2 and VMP. They must be disabled using powershell or Windows GUI (look for "Turn Windows features on or off"), if you later want to use virtualbox properly or any other software that may conflict with those feature enabled.

Steps : 
- Install rancher desktop on your machine (be sure to select dockerd container engine during installation), and reboot it after installation if required : https://rancherdesktop.io/
- After installation, clone this git repository somewhere on your PC or download the zip and extract it.
- Start Rancher Desktop so that it can start the WSL environment that runs the docker daemon. (You can close the window as a background process is running and keeping alive the environment) 
- Run the script to start yak from the rancher directory inside the repository's directory  :
``` powershell
yak-on-windows\rancher>  powershell -ExecutionPolicy Bypass ./start_yak.ps1
```
This scripts creates an "inventory" folder on the host and runs the YaK container by mounting this directory at the following path on the container : /workspace/yak/configuration/infrastructure.


- If it works properly, you'll get a shell inside the yak container like this and can start using it : 
``` bash
INFO: type 'yakhelp' to display the help of YAK

yak@a9f0fe223b0b:~/yak$ aig
@all:
  |--@ungrouped:
yak@a9f0fe223b0b:~/yak$ 
```



### Run YaK using a virtualbox virtual machine 
This method is the preferred method if you are a virtualbox user and want to keep your other virtualbox VMs running properly.

Prerequisites :  
   - Requires a CPU that allows nested virtualization. If your PC is recent, this is most likely the case.

Installation : 

- Install Vagrant if you don't already have it. Vagrant is a wrapper for Virtualbox / other hypervisors and will allow us to quickly create a VM for YaK by pulling a virtual machine image and configuring it : 
    - Option 1 : Use winget package manager :
        > winget install Hashicorp.Vagrant 
    - Option 2 : Download latest installer : 
        > https://developer.hashicorp.com/vagrant/downloads

- Install vagrant vb-guest plugin, it allows to install guest additions automatically if the vagrant box guest addition version doesn't match your virtualbox version : 
  > vagrant plugin install vagrant-vbguest

- Clone this git repository somewhere on your machine, open a powershell and cd into it.

- Create the yak-on-windows/vagrant/inventory folder. The reason why it is not already created is that git can't track empty folders.

- Install the yak virtual machine using the Vagrantfile inside this repository (This will take some tine as it downloads a rocky 9 VM image, installs docker and vbox guest additions on it) :
> yak-on-windows\vagrant> vagrant up

- Here are the parameters of the VM (which can be edited by modifying the Vagrantfile) :
```
        - IP address : 10.10.10.100 (reachable from host as vagrant automatically takes care of the host network configuration)
        - user : yak
        - password : yak
```
- ssh into the newly created virtual machine from a powershell terminal (or other software such as Mobaxterm) : 
    > ssh yak@10.10.10.100
    > yak@10.10.10.100's password: yak

-  Once you are connected to the VM, launch yak and start using it with the following script : 
    > sh start_yak.sh

- You can share files using the yak-on-windows\vagrant\inventory folder on your host, which is automatically mounted on the /home/yak/yak/inventory path in the VM. This path is available in the yak container at /workspace/yak/configuration/infrastructure path when you run it.

- You can turn off the VM using the vagrant stop command or using virtualbox CLI or GUI. If you need to edit parameters set in the Vagrantfile, such as the ip address of the VM, you can use "vagrant reload" command to apply those new parameters without deleting the VM and wait a long time to reinstall everything.


## Authors and acknowledgment
Ryan BADAÏ 
