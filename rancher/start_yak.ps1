mkdir inventory -erroraction 'silentlycontinue'
$env:MY_LOCAL_CONFIGURATION_DIR = $PSScriptRoot + "\inventory"
docker run -it --rm --name yak --pull always -v ${env:MY_LOCAL_CONFIGURATION_DIR}:/workspace/yak/configuration/infrastructure -e YAK_ENABLE_SUDO=true registry.gitlab.com/yak4all/yak:latest bash
